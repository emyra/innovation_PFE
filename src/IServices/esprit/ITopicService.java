/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IServices.esprit;

import com.esprit.entity.Soutenance;
import com.esprit.entity.Topic;
import com.esprit.entity.Subject;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.ObservableList;

/**
 *
 * @author Kaisser
 */
public interface ITopicService  {
     void add(Topic t);
     void edit(Topic topic);
     void delete(int topicId);
     List<Topic> getAll();

     Topic getTopicById(int id);

    /*public Subject afficher(int id  );
     public ArrayList<Subject> afficherparId(int id);
    public void modifier(Subject r, int  nbrreserv);
    
    public void AffichageReservations(Connection c , ObservableList<Subject>listeReservation);
    
     public void Confirmee(Subject r , int nbr);
                            public void NonConfirmee(int id);*/
    
    
    
}
