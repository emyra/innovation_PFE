/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IServices.esprit;

import com.esprit.services.*;
import com.esprit.entity.Quizz;
import com.esprit.entity.Utilisateur;
import java.util.List;

/**
 *
 * @author hp
 */
public interface IQuizService {
    
     /*public void add(Quizz q);
        public void edit(Quizz q);
            public void delete(Quizz q);*/
                public Quizz getQuizzById(int id);
                public List<Quizz> getAll();
                
}
