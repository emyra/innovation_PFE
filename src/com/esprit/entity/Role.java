/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esprit.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author Hppp
 */
@Entity
public class Role implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
  
    private String description;
    private String pass;
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

  

    

    @Override
    public String toString() {
        return "com.esprit.entity.Role[ id=" + id + " ]";
    }

    public String getDescription() {
        return description;
    }

    public String getPass() {
        return pass;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
   
}
