/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esprit.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author sahar
 */
@Entity
@Table
public class CandidateResponse {
    
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name= "id")
   private int id;
   
   @Column(name= "fk_candidateQuizz_id")
   private int candidateQuizzId;
   
   @Column(name= "questionId")
   private int questionId;
   
   @Column(name= "result")
   private int result;
   
    public int getId(){
        return id;
    }
    
    public void setId(int id){
        this.id = id;
    }
    
    public int getCandidateQuizzId(){
        return candidateQuizzId;
    }
    
    public void setCandidateQuizzId(int id){
        this.candidateQuizzId = id;
    }
    
    public int getQuestionId(){
        return questionId;
    }
    
    public void setQuestionId(int id){
        this.questionId = id;
    }
    
    public int getResult(){
        return result;
    }
    
    public void setResult(int result){
        this.result = result;
    }

}
