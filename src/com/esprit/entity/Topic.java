/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esprit.entity;

import java.util.List;

/**
 *
 * @author sahar
 */
public class Topic {
    
    
    private int id;
    private String title;
    private String description;
    private String tag;
    private Utilisateur creator;
    private int creatorId;
    private List<Subject> requiredSubjects;
    private List<CandidateApplication> candidateApplications;
      private Quizz quizz;

    private int quizzId;
    private String creatorName;

    public Topic() {
    }

    public Topic(int id, String title, String description, String tag, Utilisateur creator, List<Subject> requiredSubjects, List<CandidateApplication> candidateApplications, int quizzId) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.tag = tag;
        this.creator = creator;
        this.requiredSubjects = requiredSubjects;
        this.candidateApplications = candidateApplications;
        this.quizzId = quizzId;
    }

    @Override
    public String toString() {
        return "Topic{" + "id=" + id + ", title=" + title + ", description=" + description + ", tag=" + tag + ", creator=" + creator + ", requiredSubjects=" + requiredSubjects + ", candidateApplications=" + candidateApplications + ", quizzId=" + quizzId + '}';
    }

    
    
    
    
    
    
    
    

  
    
   
    


    
    
    
    
    
    
    
    public Quizz getQuizz(){
        return this.quizz;
    }
    
    public void setQuizz(Quizz quizz){
        this.quizz = quizz;
    }

    
    
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Utilisateur getCreator() {
        return creator;
    }

    public void setCreator(Utilisateur creator) {
        this.creator = creator;
    }

    public List<Subject> getRequiredSubjects() {
        return requiredSubjects;
    }

    public void setRequiredSubjects(List<Subject> requiredSubjects) {
        this.requiredSubjects = requiredSubjects;
    }

    public List<CandidateApplication> getCandidateApplications() {
        return candidateApplications;
    }

    public void setCandidateApplications(List<CandidateApplication> candidateApplications) {
        this.candidateApplications = candidateApplications;
    }

    public int getQuizzId() {
        return quizzId;
    }

    public void setQuizzId(int quizzId) {
        this.quizzId = quizzId;
    }
    
    public int getCreatorId(){
        return creatorId;
    }
    
    public void setCreatorId(int id){
        this.creatorId = id;
    }
    
    
    public String getCreatorName(){
        if(creator!=null)
            return creator.getPrenom() + " " + creator.getNom();
        return "";
    }
    
}
