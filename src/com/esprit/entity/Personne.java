package com.esprit.entity;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



/**
 *
 * @author Djoo
 */
public class Personne {
    
    private int id ;
    private String nom;
    private String prenom;
    
    public Personne() {};
    public Personne(String nom,String prenom)
    { 
     this.nom=nom;
     this.prenom=prenom ;
    }
    public Personne(int id, String nom,String prenom)
    { this.id=id;
     this.nom=nom;
     this.prenom=prenom ;
    }

    /**
     *
     * @return
     */
    public int getid() {
        return id;
    }

    public String getnom() {
        return nom;
    }

    public String getprenom() {
        return prenom;
    }

    public void setid(int id) {
        this.id = id;
    }

    public void setnom(String nom) {
        this.nom = nom;
    }

    public void setprenom(String prenom) {
        this.prenom = prenom;
    }

    @Override
    public String toString() {
        return "Personne{" + "id=" + id + ", nom=" + nom + ", prenom=" + prenom + '}';
    }
  
    
}
