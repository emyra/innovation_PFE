/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esprit.services;

import IServices.esprit.ISoutenanceService;
import IServices.esprit.IUtilisateurService;
import com.esprit.entity.Quizz;

import com.esprit.entity.Soutenance;
import com.esprit.entity.Utilisateur;
import com.esprit.utils.DATASOURCE;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.DataSource;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Kaisser
 */
public class SoutenanceService implements ISoutenanceService{
     
    private Connection cnx;
    
    
    public static SoutenanceService instance;
    public SoutenanceService() {
        cnx = DATASOURCE.getInstance().getCnx();
    }
    
    public static SoutenanceService getInstance(){
        if(instance == null)
            instance = new SoutenanceService();
        return instance;
            
    
        
    }
 
    
    
    
//* @Override
    public List<Soutenance> listeSoutenance() {
        
        
         String sql = "SELECT * FROM soutenance ";
        List<Soutenance> Soutenances = new ArrayList<>();
        try (PreparedStatement statement = cnx.prepareStatement(sql)) {
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Soutenance Soutenance = new Soutenance();
                Soutenance.setId(resultSet.getInt("id"));
                Soutenance.setCandidateId(resultSet.getInt("candidateId"));
                Soutenance.setSupervisor(resultSet.getInt("supervisor"));
                Soutenance.setTopic(resultSet.getInt("topic"));
//                Soutenance.setList(resultSet.getList("jury"));
                Soutenance.setDate(resultSet.getString("date"));
                Soutenance.setSalle(resultSet.getString("salle"));
                Soutenance.setResult(resultSet.getString("result"));


               
               Soutenances.add(Soutenance);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return Soutenances;
    }

    @Override
    public void add(Soutenance d) {
        String resq = "insert into `soutenance`( candidateId, supervisor, topic, `date`,"
                + " `result`, `salle`) values (?,?,?,?,?,?)";

        try {
            PreparedStatement ps = cnx.prepareStatement(resq);
           
            ps.setInt(1, d.getCandidateId());
            ps.setInt(2, d.getSupervisor());
            ps.setInt(3, d.getTopic());
       //     ps.setInt(6, d.getJury());
            ps.setString(4,  d.getDate());
            ps.setString(5, d.getResult());
            ps.setString(6, d.getSalle());

           
            

            ps.executeUpdate();
            System.out.println("Soutenance ajouter");
        } catch (SQLException ex) {
            Logger.getLogger(SoutenanceService.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("erreur");

        }

    }

    @Override
    public void edit(Soutenance d, int id) {
        show(id);
        String resq = " UPDATE `soutenance` SET `id`=?,"
                + "`candidateId`=?,`supervisor`=?,`topic`=?,`jury`=?,`date`=?,"
                + "`result`=?,`salle`=? WHERE id = ? ";

        try {
            PreparedStatement ps = cnx.prepareStatement(resq);

            ps.setInt(1, d.getId());
            ps.setInt(1, d.getCandidateId());

              ps.setInt(2, d.getId());
            ps.setInt(3, d.getCandidateId());
            ps.setInt(4, d.getSupervisor());
            ps.setInt(5, d.getTopic());
       //     ps.setInt(6, d.getJury());
            ps.setString(7,  d.getDate());
            ps.setString(8, d.getResult());
            ps.setString(9, d.getSalle());
            
            ps.executeUpdate();
            System.out.println("Soutennace modifier avec succeé");
        } catch (SQLException ex) {
            Logger.getLogger(SoutenanceService.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("erreur");
        }

    }

    @Override
    public void delete(int id) {

        String resq = " DELETE FROM soutenance WHERE  id = ? ";
        PreparedStatement ps;
        try {
            ps = cnx.prepareStatement(resq);
            ps.setInt(1, id);
            ps.executeUpdate();
            System.out.println("soutenance supprimer");
        } catch (SQLException ex) {
            Logger.getLogger(SoutenanceService.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("erreur");
        }

    }

    @Override
    public Soutenance show(int id) {
        Soutenance e = new Soutenance();
        String resq = "select * from  soutenance where id = ? ";
        try {
            PreparedStatement ps = cnx.prepareStatement(resq);
            ps.setInt(1, id);

            ResultSet r = ps.executeQuery();
            while (r.next()) { 

                e.setId(r.getInt("id"));
                e.setCandidateId(r.getInt("candidateId"));
                e.setSupervisor(r.getInt("supervisor"));
                e.setTopic(r.getInt("topic"));
//                e.setJury(r.getInt("jury"));
                e.setCandidateId(r.getInt("CandidateId"));
                e.setDate(r.getString("date"));
                e.setResult(r.getString("Result"));
                e.setSalle(r.getString("Salle"));

                
               
              

            }

        } catch (SQLException ex) {
            Logger.getLogger(SoutenanceService.class.getName()).log(Level.SEVERE, null, ex);

        }

        return e;
    }
    @Override
public ArrayList<Soutenance> showall () {

 ArrayList<Soutenance> eve = new ArrayList<Soutenance>();
        String resq = "select * from  soutenance  ";
        try {
            PreparedStatement ps = cnx.prepareStatement(resq);

            ResultSet r = ps.executeQuery();
            while (r.next()) {
                eve.add(show(r.getInt("id")));
                System.out.println(show(r.getInt("id")));
            }

        } catch (SQLException ex) {
            Logger.getLogger(SoutenanceService.class.getName()).log(Level.SEVERE, null, ex);

        }

        return eve;



}
    
    
    
    @Override
    public ArrayList<Soutenance> recherche(String id) {
        ArrayList<Soutenance> eve = new ArrayList<Soutenance>();
        String resq = "select * from  soutenance where nom like ? ";
        try {
            PreparedStatement ps = cnx.prepareStatement(resq);
            ps.setString(1, "%"+id+"%");

            ResultSet r = ps.executeQuery();
            while (r.next()) {
                eve.add(show(r.getInt("id")));
            }

        } catch (SQLException ex) {
            Logger.getLogger(SoutenanceService.class.getName()).log(Level.SEVERE, null, ex);

        }

        return eve;
    }
    

  public Soutenance getSoutenanceById(int id) {
        
        
         String sql = "SELECT * FROM soutenance "
                 + "where id = ?";
       Soutenance soutenance  = null;
        try (PreparedStatement statement = cnx.prepareStatement(sql)) {
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                soutenance = new Soutenance();
                soutenance.setId(resultSet.getInt("id"));
                soutenance.setCandidateId(resultSet.getInt("candidateId"));
                
                soutenance.setSupervisor(resultSet.getInt("supervisor"));
                soutenance.setTopic(resultSet.getInt("topic"));
                


               
               
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return soutenance;

    }

    @Override
    public List<Soutenance> listSoutenance() {
      
   
         String sql = "SELECT * FROM soutenance ";
        List<Soutenance> Soutenances = new ArrayList<>();
        try (PreparedStatement statement = cnx.prepareStatement(sql)) {
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Soutenance Soutenance = new Soutenance();
                Soutenance.setId(resultSet.getInt("id"));
                Soutenance.setCandidateId(resultSet.getInt("candidateId"));
                Soutenance.setSupervisor(resultSet.getInt("supervisor"));
                Soutenance.setTopic(resultSet.getInt("topic"));
//                Soutenance.setList(resultSet.getList("jury"));
                Soutenance.setDate(resultSet.getString("date"));
                Soutenance.setSalle(resultSet.getString("salle"));
                Soutenance.setResult(resultSet.getString("result"));


               
               Soutenances.add(Soutenance);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return Soutenances;
    }
    
    
   
    }

   
    
    
    
    

