package com.esprit.services;
import IServices.esprit.IUtilisateurService;
import com.esprit.entity.Role;
import com.esprit.entity.Soutenance;
import com.esprit.entity.Utilisateur;
import com.esprit.services.SoutenanceService;
import com.esprit.services.TopicServiceImpl;
import com.esprit.utils.DATASOURCE;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;



public class RoleService {
    
    private Connection cnx;
    private static RoleService instance;
    
    private EntityManagerFactory emf;
    private EntityManager em;
    
    private RoleService() {
        cnx = DATASOURCE.getInstance().getCnx();
        emf = Persistence.createEntityManagerFactory("pu");
        em = emf.createEntityManager();
    }
    public static RoleService getInstance(){
        if(instance == null)
            instance = new RoleService();
        return instance;
            
    
        
    }
    
    
  
    public Role getRoleById(int id) {
        Role role = new Role();
        String resq = "select * from  role where id = ? ";
        try {
            PreparedStatement ps = cnx.prepareStatement(resq);
            ps.setInt(1, id);

            ResultSet r = ps.executeQuery();
            while (r.next()) { 

                role.setId(r.getInt("id"));
                role.setDescription(r.getString("description"));
                role.setPass(r.getString("pass"));
                

                
               
              

            }

        } catch (SQLException ex) {
            Logger.getLogger(RoleService.class.getName()).log(Level.SEVERE, null, ex);

        }

        return role;
    
    }

     public Role getRoleByNameAndPass(String name, String pass) {
        Role role=new Role();
        String resq = "select r.*  from  role r , utilisateur u where u.name=? and r.pass=? and r.id=u.role";
        try {
            PreparedStatement ps = cnx.prepareStatement(resq);
            ps.setString(1, name);
            ps.setString(2,pass);

            ResultSet r = ps.executeQuery();
           

                role.setId(r.getInt("id"));
                role.setDescription(r.getString("description"));
                role.setPass(r.getString("pass"));
                

                
               
              

            

        } catch (SQLException ex) {
            Logger.getLogger(RoleService.class.getName()).log(Level.SEVERE, null, ex);

        }

        return role;
    
    }
       
        
  
  
}