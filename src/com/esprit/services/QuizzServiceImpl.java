/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esprit.services;


import IServices.esprit.IQuizService;
import com.esprit.entity.CandidateQuizz;
import com.esprit.entity.CandidateResponse;
import com.esprit.entity.Choice;
import com.esprit.entity.Question;
import com.esprit.entity.Quizz;
import com.esprit.entity.Topic;
import com.esprit.utils.DATASOURCE;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Emyra
 */
public class QuizzServiceImpl implements IQuizService{
    
    private Connection cnx;
    private EntityManagerFactory emf;
    private EntityManager em;
    private static QuizzServiceImpl instance;
    
    
    private QuizzServiceImpl() {
        cnx = DATASOURCE.getInstance().getCnx();
        emf = Persistence.createEntityManagerFactory("pu");
        em = emf.createEntityManager();
    }
    public static QuizzServiceImpl getInstance(){
        if(instance == null)
            instance = new QuizzServiceImpl();
        return instance;
            
    
        
    }
    @Override
    public List<Quizz> getAll() {
        
        
         String sql = "SELECT * FROM Quizz ";
        List<Quizz> quizzList = new ArrayList<>();
        try (PreparedStatement statement = cnx.prepareStatement(sql)) {
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Quizz quizz = new Quizz();
                quizz.setId(resultSet.getInt("id"));
                quizz.setName(resultSet.getString("name"));
                
                quizz.setDescription(resultSet.getString("description"));
               


               
               quizzList.add(quizz);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return quizzList;
    }
    
    public Quizz getQuizzById(int id){
        
        String sql = "select * from quizz where id = ?";
        
        Quizz quizz = null;
        try (PreparedStatement statement = cnx.prepareStatement(sql)) {
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                quizz = new Quizz();
                quizz.setId(resultSet.getInt("id"));
                quizz.setName(resultSet.getString("name"));
                quizz.setQuestions(getQuestionsByQuizzId(id));
                
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
       return quizz;
        
    }
    
    private List<Question> getQuestionsByQuizzId(int quizzId){
        
        String sql = "select * from question where fk_quizz_id = ?";
        
        List<Question> questions = new ArrayList<>();
        try (PreparedStatement statement = cnx.prepareStatement(sql)) {
            statement.setInt(1, quizzId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Question question = new Question();
                question.setId(resultSet.getInt("id"));
                question.setName(resultSet.getString("name"));
                question.setQuizzId(resultSet.getInt("fk_quizz_id"));
                question.setResult(resultSet.getInt("result"));
                question.setChoices(getChoicesByQuestionId(question.getId()));
                
                questions.add(question);
                
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return questions;
    }
    
    private List<Choice> getChoicesByQuestionId(int quizzId){
        
        String sql = "select * from choice where fk_question_id = ?";
        
        List<Choice> choices = new ArrayList<>();
        try (PreparedStatement statement = cnx.prepareStatement(sql)) {
            statement.setInt(1, quizzId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Choice choice = new Choice();
                choice.setId(resultSet.getInt("id"));
                choice.setLabel(resultSet.getString("label"));
                choice.setQuestionId(resultSet.getInt("fk_question_id"));
                choices.add(choice);
                
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return choices;
    }
    
    
    
    public void saveCandidateResponse(CandidateQuizz response){
        
        
        String resq = "insert into `candidateQuizz`( candidateId, topicId, quizzId, score "
                + ") values (?,?,?,?)";

        try {
            PreparedStatement ps = cnx.prepareStatement(resq);
           
            ps.setInt(1, response.getCandidateId());
            ps.setInt(2, response.getTopicId());
            ps.setInt(3, response.getQuizzId());
            ps.setInt(4, response.getScore());

           
            

            ps.executeUpdate();
            System.out.println("candiateQuizz added succesfully! ");
        } catch (SQLException ex) {
            Logger.getLogger(QuizzServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("ERROR");

        }
         
    }
}
