package com.esprit.services;
import IServices.esprit.IUtilisateurService;
import com.esprit.entity.Soutenance;
import com.esprit.entity.Utilisateur;
import com.esprit.services.SoutenanceService;
import com.esprit.services.TopicServiceImpl;
import com.esprit.utils.DATASOURCE;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;



public class UtilisateurService implements IUtilisateurService{
    
    private Connection cnx;
    private static UtilisateurService instance;
    
    private EntityManagerFactory emf;
    private EntityManager em;
    
    private UtilisateurService() {
        cnx = DATASOURCE.getInstance().getCnx();
        emf = Persistence.createEntityManagerFactory("pu");
        em = emf.createEntityManager();
    }
    public static UtilisateurService getInstance(){
        if(instance == null)
            instance = new UtilisateurService();
        return instance;
            
    
        
    }
    @Override 
    public Utilisateur login(String name, String password){
        Utilisateur user = null;
        String resq = "select * from  utilisateur where username = ? "
                + "and password = ? ";
        
        try {
            PreparedStatement ps = cnx.prepareStatement(resq);
            ps.setString(1, name);
            ps.setString(2, password);

            ResultSet r = ps.executeQuery();
            while (r.next()) { 
                user= new Utilisateur();
                user.setId(r.getInt("id"));
                user.setNom(r.getString("nom"));
                user.setPrenom(r.getString("prenom"));
                user.setMail(r.getString("mail"));
                user.setRole(r.getString("role"));
                

                
               
              

            }

        } catch (SQLException ex) {
            Logger.getLogger(UtilisateurService.class.getName()).log(Level.SEVERE, null, ex);

        }
        
        return user;
    }
    
    @Override
    public Utilisateur getUserById(int id) {
        Utilisateur user = new Utilisateur();
        String resq = "select * from  utilisateur where id = ? ";
        try {
            PreparedStatement ps = cnx.prepareStatement(resq);
            ps.setInt(1, id);

            ResultSet r = ps.executeQuery();
            while (r.next()) { 

                user.setId(r.getInt("id"));
                user.setNom(r.getString("nom"));
                user.setPrenom(r.getString("prenom"));
                user.setMail(r.getString("mail"));
                

                
               
              

            }

        } catch (SQLException ex) {
            Logger.getLogger(UtilisateurService.class.getName()).log(Level.SEVERE, null, ex);

        }

        return user;
    
    }
    public List<Utilisateur> listeUtilisateur() {
        return null;
    
    }
     
    public void add(Utilisateur d) {
        String resq = "insert into `utlisateur`( id, nom, topic, `prenom`,`nom`) values (?,?,?)";

        try {
            PreparedStatement ps = cnx.prepareStatement(resq);
           
            ps.setInt(1, d.getId());
            ps.setString(2, d.getNom());
            ps.setString(3, d.getPrenom());
       //     ps.setInt(6, d.getJury());
           

           
            

            ps.executeUpdate();
            System.out.println("Utilisateur ajouter");
        } catch (SQLException ex) {
            Logger.getLogger(UtilisateurService.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("erreur");

        }

    }

}