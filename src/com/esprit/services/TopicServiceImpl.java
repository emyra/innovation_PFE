/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esprit.services;

import IServices.esprit.ITopicService;
import com.esprit.entity.Soutenance;
import com.esprit.entity.Topic;
import com.esprit.utils.DATASOURCE;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Emyra
 */
public  class TopicServiceImpl implements ITopicService{
    
    private Connection cnx;
    
    
    private static TopicServiceImpl instance;
    
    public TopicServiceImpl() {
        cnx = DATASOURCE.getInstance().getCnx();
    }
    
    public static TopicServiceImpl getInstance(){
        if(instance == null)
            instance = new TopicServiceImpl();
        return instance;
            
    
        
    }

    
    

     @Override
    public List<Topic> getAll() {
        
        
         String sql = "SELECT * FROM Topic ";
        List<Topic> topicList = new ArrayList<>();
        try (PreparedStatement statement = cnx.prepareStatement(sql)) {
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Topic topic = new Topic();
                topic.setId(resultSet.getInt("id"));
                topic.setTitle(resultSet.getString("title"));
                
                topic.setDescription(resultSet.getString("description"));
                topic.setTag(resultSet.getString("tag"));
                topic.setCreatorId(resultSet.getInt("fkCreatorId"));
                topic.setQuizzId(resultSet.getInt("fk_quizz_id"));


               
               topicList.add(topic);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return topicList;
    }
    
    @Override
    public Topic getTopicById(int id) {
        
        
         String sql = "SELECT * FROM Topic "
                 + "where id = ?";
        Topic topic  = null;
        try (PreparedStatement statement = cnx.prepareStatement(sql)) {
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                topic = new Topic();
                topic.setId(resultSet.getInt("id"));
                topic.setTitle(resultSet.getString("title"));
                
                topic.setDescription(resultSet.getString("description"));
                topic.setTag(resultSet.getString("tag"));
                topic.setCreatorId(resultSet.getInt("fkCreatorId"));
                topic.setQuizzId(resultSet.getInt("fk_quizz_id"));


               
               
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return topic;

    }
    
    
    
    @Override
    public void add(Topic topic) {
        String resq = "insert into `topic`( title, description, tag, fkCreatorId, fk_quizz_id "
                + ") values (?,?,?,?,?)";

        try {
            PreparedStatement ps = cnx.prepareStatement(resq);
           
            ps.setString(1, topic.getTitle());
            ps.setString(2, topic.getDescription());
            ps.setString(3, topic.getTag());
            ps.setInt(4, topic.getCreatorId());
            ps.setInt(5, topic.getQuizzId());

           
            

            ps.executeUpdate();
            System.out.println("Topic added succesfully! ");
        } catch (SQLException ex) {
            Logger.getLogger(TopicServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("ERROR");

        }

    }

    
    @Override
    public void edit(Topic topic) {
        
        String resq = " UPDATE `topic` SET "
                + "`title`=?,`description`=?,`tag`=? , `fk_quizz_id`=? "
                + "WHERE id = ? ";

        try {
            PreparedStatement ps = cnx.prepareStatement(resq);

            ps.setString(1, topic.getTitle());
            ps.setString(2, topic.getDescription());

              ps.setString(3, topic.getTag());
            ps.setInt(4, topic.getQuizzId());
            ps.setInt(5, topic.getId());
            
            
            ps.executeUpdate();
            System.out.println("Topic modified successfully");
        } catch (SQLException ex) {
            Logger.getLogger(TopicServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("ERROR");
        }

    }
    @Override
    public void delete(int topicId){
        String resq = " DELETE FROM topic WHERE  id = ? ";
        PreparedStatement ps;
        try {
            ps = cnx.prepareStatement(resq);
            ps.setInt(1, topicId);
            ps.executeUpdate();
            System.out.println("Topic id ==> " + topicId + " was deleted");
        } catch (SQLException ex) {
            Logger.getLogger(TopicServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("ERROR");
        }
    }
    
}
