/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication1.esprit.controller;


import com.esprit.utils.DATASOURCE;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Kaisser
 */
public class BarChartquizzController implements Initializable {

    @FXML
    private BarChart<String,Integer> barChartquizz;
    private Connection c;
    @FXML
    private AnchorPane eve;
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            // TODO
            loadChart();
        } catch (SQLException ex) {
            Logger.getLogger(BarChartController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }    

  
    private void loadChart() throws SQLException {
        
        
             String query ="SELECT `id`,`id` FROM `quizz` ORDER BY `id` ";
            XYChart.Series<String,Integer> series = new XYChart.Series<>();
            
            
            c=DATASOURCE.getInstance().getCnx();
            ResultSet rs = c.createStatement().executeQuery(query);
            
            while (rs.next()) {
              series.getData().add(new XYChart.Data<>(rs.getString(1), rs .getInt(2)));
                
            }
            barChartquizz.getData().add(series);
       
        
        
        
    }

    @FXML
    private void retour(ActionEvent event) throws IOException {
              AnchorPane a = (AnchorPane)  FXMLLoader.load(getClass().getResource("/javafxapplication1/Statistique.fxml"));
        eve.getChildren().setAll(a);
        
        
    }
}
