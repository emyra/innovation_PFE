/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication1.esprit.controller;

import com.esprit.entity.CandidateQuizz;
import com.esprit.entity.CandidateResponse;
import com.esprit.entity.Choice;
import com.esprit.entity.Question;
import com.esprit.entity.Quizz;
import com.esprit.entity.Topic;
import com.esprit.entity.Utilisateur;
import com.esprit.services.MailServiceImpl;
import com.esprit.services.QuizzServiceImpl;
import com.esprit.services.TopicServiceImpl;
import com.esprit.services.UtilisateurService;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Tab;
import javafx.scene.control.TitledPane;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

/**
 *
 * @author Emyra
 */
public class QuizzController implements Initializable{
    
  
    
    @FXML
    private VBox quizzContainer;
    
    @FXML
    private TitledPane quizzPane;
    
    List<ToggleGroup> groups = new ArrayList<>();
    public Topic currentTopic = null;
    
    
    public Tab topicListTab;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        
            //Parent root = FXMLLoader.;
            //Topic currentTopic = (Topic) root.getUserData();
            //currentTopic = TopicServiceImpl.getInstance().getTopicById(1);
            if(currentTopic != null && currentTopic.getQuizzId()!= 0){
                Quizz quizz = QuizzServiceImpl.getInstance().getQuizzById(currentTopic.getQuizzId());
                currentTopic.setQuizz(quizz);
                generateQuizz(currentTopic.getQuizz());
            }

            
       
         
        
    }
    
    private void generateQuizz(Quizz quizz){
        quizzContainer.setSpacing(20);
        
        if(quizz != null){
            final Label quizzTitle = new Label(quizz.getName());
            quizzPane.setText(quizz.getName());
            
            
            for(Question question : quizz.getQuestions()){
                VBox questionContainer = new VBox(5);
                final Label questionLabel = new Label(question.getName());
                questionLabel.setId(buildLabelId(question.getId()));
                questionLabel.setFont(new Font("Arial", 12));
                questionContainer.getChildren().add(questionLabel);
                //quizzContainer.getChildren().add(questionLabel);
                final ToggleGroup group = new ToggleGroup();
                //mark the group with the question
                group.setUserData(question);
                groups.add(group);
                for(Choice choice: question.getChoices()){
                    
                    RadioButton rb1 = new RadioButton(choice.getLabel());
                    //mark the radioButton with the choice 
                    rb1.setUserData(choice);
                    rb1.setToggleGroup(group);
                    questionContainer.getChildren().add(rb1);
                    //quizzContainer.getChildren().add(rb1);
            
                    
                }
                quizzContainer.getChildren().add(questionContainer);
                
            }
            HBox buttonContainer = new HBox();
            
            Button submitButton = new Button("submit");
            buttonContainer.getChildren().add(submitButton);
            buttonContainer.setAlignment(Pos.CENTER);
            quizzContainer.getChildren().add(buttonContainer);
            
            submitButton.setOnAction(new EventHandler<ActionEvent>(){
                @Override
                public void handle(ActionEvent e) {
                    submitHandler(e);
                }
            });
        }

    }
    
    private void submitHandler(ActionEvent e){
        int countCorrectResponse = 0;
        int nbQuestions = 0;
        for(ToggleGroup group : groups){
            //retrieve the question 
            Question question = (Question) group.getUserData();
            nbQuestions++;
            Toggle selected = group.getSelectedToggle();
            //retrieve the selected choice
            Choice choice = (Choice) selected.getUserData();
            System.out.println("question id  ==> " + question.getName()
                            +"choice id ==> " + choice.getLabel());
            if(validateResponse(question, choice.getId()))
                 countCorrectResponse++;
                         
                
            
        }
        int score = getCandidateScore(nbQuestions, countCorrectResponse);
        CandidateQuizz candidateQuizz = buildCandidateQuizz(score);
        QuizzServiceImpl.getInstance().saveCandidateResponse(candidateQuizz);
        Utilisateur user =  UtilisateurService.getInstance().getUserById(3);
        boolean sent = MailServiceImpl.getInstance().sendQuizzResult(candidateQuizz.getScore(), user.getMail());
        
    }
    
    
    private CandidateQuizz buildCandidateQuizz(int score){
        CandidateQuizz candiateQuizz = new CandidateQuizz();
        candiateQuizz.setCandidateId(1);
        candiateQuizz.setTopicId(currentTopic.getId());
        candiateQuizz.setQuizzId(currentTopic.getQuizzId());
        candiateQuizz.setScore(score);
        return candiateQuizz;
        
    }
    private int getCandidateScore(int nbQuestions, int nbResponses){
        return (nbResponses * 100) / nbQuestions;
    }
    
    private boolean validateResponse(Question question, int choiceId){
        return (question.getResult() == choiceId)? true: false;
    }
    private String buildLabelId(int questionId){
        return "label_"+ questionId;
    }
    
    private int getQuestionIdFromLabel(String label){
        int questionId = 0;
        String[] arr = label.split("_");
        if(arr !=null && arr.length == 2){
            questionId = new Integer(arr[1]).intValue();
        }
        return questionId;
    }
    
}
