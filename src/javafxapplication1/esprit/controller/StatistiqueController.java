/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication1.esprit.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author its48ha1
 */
public class StatistiqueController implements Initializable {

    @FXML
    private Label lbl;
    @FXML
    private Button acceuil;
    @FXML
    private Button acceuil2;
    @FXML
    private Button acceuil1;
    @FXML
    private Button acceuil3;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void stat1(ActionEvent event) throws IOException {
         Parent   page = FXMLLoader.load(getClass().getResource("/javafxapplication1/BarChart.fxml"));

        Scene scene = new Scene(page);
       Stage stage;
        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    private void userstat(ActionEvent event) throws IOException {
         Parent   page = FXMLLoader.load(getClass().getResource("/javafxapplication1/BarChartUser.fxml"));

        Scene scene = new Scene(page);
       Stage stage;
        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    private void topicstat(ActionEvent event) throws IOException {
         Parent   page = FXMLLoader.load(getClass().getResource("/javafxapplication1/BarChartTopic.fxml"));

        Scene scene = new Scene(page);
       Stage stage;
        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    private void quizstat(ActionEvent event) throws IOException {
         Parent   page = FXMLLoader.load(getClass().getResource("/javafxapplication1/BarChartquiz.fxml"));

        Scene scene = new Scene(page);
       Stage stage;
        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }
    
}
