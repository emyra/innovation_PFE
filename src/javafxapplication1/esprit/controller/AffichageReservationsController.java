/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication1.esprit.controller;

import com.esprit.entity.Soutenance;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.jfoenix.controls.JFXButton;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.model.Charge;
import com.stripe.net.RequestOptions;
import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;
import javafx.scene.image.ImageView;
import javax.mail.internet.MimeBodyPart;


/**
 * FXML Controller class
 *
 * @author Kaisser
 */
public class AffichageReservationsController implements Initializable {

    @FXML
    private TableView<Soutenance> table;
    @FXML
    private TableColumn<Soutenance, Integer> nbrreserv;
    @FXML
    private TableColumn<Soutenance, String> confirmation;
    @FXML
    private TableColumn<Soutenance, Integer> id;
  
    @FXML
    private Button actualiser;
    @FXML
    private TextField searchReserv;
    @FXML
    private Label labelnbrreserv;
    @FXML
    private Label labelconfirmation;
    @FXML
    private Label labelid;
    @FXML
    private Label labeliduser;
    @FXML
    private Label labelidevenement;
    @FXML
    private Label labeldate;
    @FXML
    private Button imprimer;
    @FXML
    private JFXButton payer;
    
    
    
    
     private ObservableList<Soutenance> listeReservations;
     
     
    
    
   
   private Connection c;
    @FXML
    private TableColumn<Soutenance, String> date;
    @FXML
    private Label nomeve;
    @FXML
    private Label nomuser;
    @FXML
    private ImageView image;
    @FXML
    private Label labelnbrreserv1;
    @FXML
    private Label labelconfirmation1;
    @FXML
    private AnchorPane eve;
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
         
      

    }    

    @FXML
    private void imprimerReservation(ActionEvent event) {
        if(labelconfirmation.getText().equalsIgnoreCase("CONFIRMEE")){
            
        
        javafx.scene.image.Image img3 = new javafx.scene.image.Image("/Picampers/Image/17857338_10212448657988465_1114410539_n.png");
        Notifications notificationsBuilder = Notifications.create()
                .title("Impression de La Reservation")
                .text("Impression effectué")
                .graphic(new ImageView(img3))
                .hideAfter(Duration.seconds(5))
                .position(Pos.BASELINE_RIGHT)
                .onAction(new EventHandler<ActionEvent>() {
               @Override
               public void handle(ActionEvent event) {
                   System.out.println("Cliquer sur la notification");
               }
           });
        notificationsBuilder.darkStyle();
        notificationsBuilder.show();
        Document doc = new Document(PageSize.A4);
        try {
            try {
                PdfWriter.getInstance(doc, new FileOutputStream("C:\\Users\\Kaisser\\Desktop\\Reservation.pdf"));
                doc.open();
               
                Image img = Image.getInstance("C:\\Users\\Kaisser\\Desktop\\footer-logo.png"); 
                Image img1 = Image.getInstance("C:\\Users\\Kaisser\\Desktop\\Logo_ESPRIT_Ariana.png.jpg");
                img.scaleAbsoluteWidth(80);
                img1.scaleAbsoluteWidth(80);
                img.scaleAbsoluteHeight(80);
                img1.scaleAbsoluteHeight(80);
                img.setAlignment(Image.LEFT);
                img1.setAlignment(Image.RIGHT);
                doc.add(img);
                doc.add(img1);
                
                
                
                
               
                
                
                doc.add(new Paragraph("  "));
                doc.add(new Paragraph("RECU DE RESERVATION:"));
                doc.add(new Paragraph("  "));
                
                PdfPTable table = new PdfPTable(6);
                table.setWidthPercentage(100);
                PdfPCell cell;
                
                 cell = new PdfPCell(new Phrase("Reservation",FontFactory.getFont("1",9)));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
                table.addCell(cell);
                
                cell = new PdfPCell(new Phrase("Utilisateur",FontFactory.getFont("1",9)));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
                table.addCell(cell);
                
                cell = new PdfPCell(new Phrase("Evenement",FontFactory.getFont("1",9)));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
                table.addCell(cell);
               
                
                cell = new PdfPCell(new Phrase("Date de Reservation",FontFactory.getFont("1",9)));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
                table.addCell(cell);
                
                cell = new PdfPCell(new Phrase("Nombre de Reservation",FontFactory.getFont("1",9)));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
                table.addCell(cell);
                
                cell = new PdfPCell(new Phrase("Confirmation",FontFactory.getFont("1",9)));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
                table.addCell(cell);
                
                /////////////////////////////////////////////////////////////////////
                
                
               cell = new PdfPCell(new Phrase(labelid.getText().toString(),FontFactory.getFont("1",11)));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                
                table.addCell(cell);
                
                cell = new PdfPCell(new Phrase(labeliduser.getText().toString(),FontFactory.getFont("1",11)));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
               
                table.addCell(cell);
                
                cell = new PdfPCell(new Phrase(labelidevenement.getText().toString(),FontFactory.getFont("1",11)));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                
                table.addCell(cell);
                
                cell = new PdfPCell(new Phrase(labeldate.getText().toString(),FontFactory.getFont("1",11)));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
               
                table.addCell(cell);
                
                cell = new PdfPCell(new Phrase(labelnbrreserv.getText().toString(),FontFactory.getFont("1",11)));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                
                table.addCell(cell);
                
                cell = new PdfPCell(new Phrase(labelconfirmation.getText().toString(),FontFactory.getFont("1",11)));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                
                table.addCell(cell);
                
             
                
                doc.add(table);
                
                 doc.add(new Paragraph(" "));
                doc.add(new Paragraph("Signature:"));
                doc.add(new Paragraph(" "));
                doc.add(new Paragraph("Date de la Signature:"));
                
                
                 doc.add(new Paragraph(" "));
                  doc.add(new Paragraph(" "));
                
                 Image img2 = Image.getInstance("C:\\Users\\Kaisser\\Desktop\\promo-ClientLD.jpg");
                img2.scaleAbsoluteWidth(100);
                img2.scaleAbsoluteHeight(100);
                img2.setAlignment(Image.ALIGN_CENTER);
                doc.add(img2);
                
                doc.close();
                
                Desktop.getDesktop().open(new File("C:\\Users\\Kaisser\\Desktop\\Reservation.pdf"));
                
                
                
            } catch (FileNotFoundException ex) {
                Logger.getLogger(javafxapplication1.esprit.controller.AffichageReservationsController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (BadElementException ex) {
                Logger.getLogger(javafxapplication1.esprit.controller.AffichageReservationsController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(javafxapplication1.esprit.controller.AffichageReservationsController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (DocumentException ex) {
            Logger.getLogger(javafxapplication1.esprit.controller.AffichageReservationsController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }else{javafx.scene.image.Image img3 = new javafx.scene.image.Image("/Picampers/Image/rsz_kreuz-button.png");
        Notifications notificationsBuilder = Notifications.create()
                .title("Impression de La Reservation Non Effectuée")
                .text("La Reservation est en cours de Traitement")
                .graphic(new ImageView(img3))
                .hideAfter(Duration.seconds(5))
                .position(Pos.BASELINE_RIGHT)
                .onAction(new EventHandler<ActionEvent>() {
               @Override
               public void handle(ActionEvent event) {
                   System.out.println("Cliquer sur la notification");
               }
           });
        notificationsBuilder.darkStyle();
        notificationsBuilder.show();
            
            
        }
    }
   

    @FXML
    private void Retour(ActionEvent event) throws IOException {
          AnchorPane a = (AnchorPane)  FXMLLoader.load(getClass().getResource("/Picampers/views/ListeEvenements.fxml"));
        eve.getChildren().setAll(a);
    }

    private void setDialogStage(Stage dialogStage) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private boolean isButtonConfirmerClicked() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
