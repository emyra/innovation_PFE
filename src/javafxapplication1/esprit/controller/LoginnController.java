/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication1.esprit.controller;


import com.esprit.services.UtilisateurService;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import com.esprit.entity.Utilisateur;

/**
 * FXML Controller class
 *
 * @author hp
 */
public class LoginnController implements Initializable {

    @FXML
    private Button connecter;
    @FXML
    private PasswordField pass;
    @FXML
    private Label lbl;
    @FXML
    private TextField username;
    
    public static Utilisateur currentUser = null;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void login(ActionEvent event ) throws IOException {
       
        String userna = username.getText();
        String password = pass.getText();
        Utilisateur user = UtilisateurService.getInstance().login(userna, password);
        if(user != null){
        currentUser = user;
          Parent   page = FXMLLoader.load(getClass().getResource("/javafxapplication1/Dashboardd.fxml"));

        Scene scene = new Scene(page);
       Stage stage;
        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
        }
        
        
    }
    
}
