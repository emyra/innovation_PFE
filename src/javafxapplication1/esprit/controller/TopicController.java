/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication1.esprit.controller;

import com.esprit.entity.CandidateQuizz;
import com.esprit.entity.CandidateResponse;
import com.esprit.entity.Choice;
import com.esprit.entity.Question;
import com.esprit.entity.Quizz;
import com.esprit.entity.Topic;
import com.esprit.entity.Utilisateur;
import com.esprit.services.MailServiceImpl;
import com.esprit.services.QuizzServiceImpl;
import com.esprit.services.TopicServiceImpl;
import com.esprit.services.UtilisateurService;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Group;

import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Tab;
import javafx.scene.control.TitledPane;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

/**
 *
 * @author Emyra
 */
public class TopicController implements Initializable{
    
    public Tab topicListTab;
    
    @FXML
    private AnchorPane topicView;
    
    @FXML
    private Label topicTitle;
    
    @FXML
    private Label topicDescription;
    
    
    public Topic currentTopic = null;
    
    
    
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        
            
            if(currentTopic != null && currentTopic.getQuizzId()!= 0){
                topicTitle.setText(currentTopic.getTitle());
                topicDescription.setText(currentTopic.getDescription());
            }

            
       
         
        
    }
    
    public void viewQuizz(ActionEvent event){
        
try {
            URL url  = getClass().getClassLoader().getResource( "javafxapplication1/Quizz.fxml" );
            
            
            FXMLLoader loader = new FXMLLoader(url);

        // Create a controller instance
        QuizzController controller = new QuizzController();
        controller.currentTopic = currentTopic;
        controller.topicListTab = topicListTab;
        // Set it in the FXMLLoader
        loader.setController(controller);
            
            Parent root = loader.load();
            
            topicListTab.setContent(root);
                    
        } catch (IOException ex) {
            Logger.getLogger(TopicListController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
   
    
}
