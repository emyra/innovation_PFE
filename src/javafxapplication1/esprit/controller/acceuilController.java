/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication1.esprit.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.MenuButton;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author its48ha1
 */
public class acceuilController implements Initializable {

    @FXML
    private AnchorPane eve;
    @FXML
    private MenuButton soutenance;
    @FXML
    private Button guide;
    @FXML
    private Button propos;
    @FXML
    private Button contact;
    @FXML
    private Button description;
    @FXML
    private Button acceuil;
    @FXML
    private ImageView image2;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void mesevenements(ActionEvent event) {
    }

    @FXML
    private void apropos(ActionEvent event) throws IOException {
       Parent   page = FXMLLoader.load(getClass().getResource("/javafxapplication1/Apropos.fxml"));

        Scene scene = new Scene(page);
       Stage stage;
        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();}

    @FXML
    private void Contact(ActionEvent event) throws IOException {
    Parent   page = FXMLLoader.load(getClass().getResource("/javafxapplication1/Contacte.fxml"));

        Scene scene = new Scene(page);
       Stage stage;
        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();}

    
    }
    

