/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication1.esprit.controller;

/**
 *
 * @author asus
 */

public enum RoleEnum {
    ADMIN("ADMIN"), STUDENT("STUDENT"), SUPERVISOR("SUPERVISOR");
    
    
    private String value;
    RoleEnum(String value){
        this.value = value;
    }
    
    public String getValue(){
        return value;
    }

    
}
