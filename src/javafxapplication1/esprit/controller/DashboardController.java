/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



package javafxapplication1.esprit.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;

/**
 * FXML Controller class
 *
 * @author its48ha1
 */
public class DashboardController implements Initializable {
    
    @FXML
    public TabPane tabPane ; 
    @FXML
    public Tab topicListTab;
    @FXML
    public Tab accueilTab;
    @FXML
    public Tab soutenanceTab;
    @FXML
    public Tab statistiqueTab;
    @FXML
    public Tab soutenanceListTab;
    
    @FXML
    public TopicListController topicListViewController;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
                
        topicListViewController.topicListTab = topicListTab;
        if(LoginnController.currentUser!=null){
            String role = LoginnController.currentUser.getRole();
            if (role.equals(RoleEnum.STUDENT.getValue())){
                 tabPane.getTabs().remove(statistiqueTab);
                 tabPane.getTabs().remove(statistiqueTab);
            }else if (role.equals(RoleEnum.ADMIN.getValue())){
            
            }else if (role.equals(RoleEnum.SUPERVISOR.getValue())){
            
            }
            
        }
        

    }    
    
}
