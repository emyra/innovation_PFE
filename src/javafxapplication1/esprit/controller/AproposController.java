

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication1.esprit.controller;


import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;

/**
 * FXML Controller class
 *
 * @author hp
 */
public class AproposController implements Initializable {

    @FXML
    private AnchorPane eve;
    @FXML
    private MediaView mv;
    @FXML
    private ImageView info;
    @FXML
    private ImageView img1;
    @FXML
    private ImageView img11;
  
    private MediaPlayer mp;
   private Media me;
 
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
  String path=new File("C:\\xampp/Soutenance.mp4").getAbsolutePath();
            
            me=new Media(new File(path).toURI().toString());
            mp=new MediaPlayer(me);
            mv.setMediaPlayer(mp);
            mp.setAutoPlay(true);
            
            DoubleProperty width = mv.fitWidthProperty();
            DoubleProperty height=mv.fitHeightProperty();
            
            width.bind(Bindings.selectDouble(mv.sceneProperty(), "width"));
            width.bind(Bindings.selectDouble(mv.sceneProperty(), "heigth"));    }    


    @FXML
    private void pause(MouseEvent event) {
          mp.pause();

    
    }

    @FXML
    private void play(ScrollEvent event) {
          mp.play();

    
    }
      @FXML
    private void retour(ActionEvent event) throws IOException {
             AnchorPane a = (AnchorPane)  FXMLLoader.load(getClass().getResource("/javafxapplication1/Dashboardd.fxml"));
        eve.getChildren().setAll(a);
        
        
    }
}
