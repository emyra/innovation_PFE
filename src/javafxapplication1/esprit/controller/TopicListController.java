/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication1.esprit.controller;

import IServices.esprit.ITopicService;
import com.esprit.entity.Quizz;
import com.esprit.entity.Topic;
import com.esprit.entity.Utilisateur;
import com.esprit.services.QuizzServiceImpl;
import com.esprit.services.TopicServiceImpl;
import com.esprit.services.UtilisateurService;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.net.URL;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Callback;


/**
 * FXML Controller class
 *
 * @author its48ha1
 */
public class TopicListController implements Initializable {
    
    public Tab topicListTab;
    
    

    @FXML
    private AnchorPane anchorredirect;
    @FXML
    private TableView<Topic> topicTable;
    @FXML
    private TableColumn<Topic, String> title;
    @FXML
    private TableColumn<Topic, String> description;
    @FXML
    private TableColumn<Topic, String> tag;
    @FXML
    private TableColumn<Topic, String> creator;
    
    @FXML
    private TableColumn editCol;
    
    @FXML
    private TableColumn actionCol ;
    
   
    @FXML
    private ImageView image;
    
    public ObservableList<Topic> data = FXCollections.observableArrayList();
    
    
    private List<Quizz> quizzList = new ArrayList<>();
        
    private  List<Topic> topicList = new ArrayList<>();
    
   private Quizz selectedQuizz = null;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        
        
        quizzList =  QuizzServiceImpl.getInstance().getAll();
        
        title.setCellValueFactory(
            new PropertyValueFactory("title")
        );
        description.setCellValueFactory(
            new PropertyValueFactory("description")
        );
        tag.setCellValueFactory(
            new PropertyValueFactory("tag")
        );
        creator.setCellValueFactory(
            new PropertyValueFactory("creatorName")
        );
        addActionColumn();
        
        
       
        topicTable.setRowFactory( tv -> {
            TableRow<Topic> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
            if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                Topic rowData = row.getItem();
                System.out.println(rowData);
               
                redirectToTopicView(rowData);
               }
            });
            return row ;
        });
        getAllTopic();
        
        
        
    }    
     private void addActionColumn(){
         actionCol = new TableColumn("Action");
        actionCol.setCellValueFactory(new PropertyValueFactory<>("DUMMY"));

        Callback<TableColumn<Topic, String>, TableCell<Topic, String>> cellFactory
                =  
                new Callback<TableColumn<Topic, String>, TableCell<Topic, String>>() {
            @Override
            public TableCell call(final TableColumn<Topic, String> param) {
                final TableCell<Topic, String> cell = new TableCell<Topic, String>() {

                    final Button deleteBtn = new Button("Delete");
                    final Button editBtn = new Button("Edit");
                    HBox pane = new HBox(editBtn, deleteBtn);
                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                            setText(null);
                        } else {
                            deleteBtn.setOnAction(event -> {
                                Topic topic = getTableView().getItems().get(getIndex());
                                showConfirmationDialog(topic);
                            });
                            editBtn.setOnAction(event -> {
                                Topic topic = getTableView().getItems().get(getIndex());
                                topic = TopicServiceImpl.getInstance().getTopicById(topic.getId());
                                showCreateEditDialog(topic);
                               // editTopic(topic);
                            });
                            
                            setGraphic(pane);
                            setText(null);
                        }
                    }
                };
                return cell;
            }
        };

        actionCol.setCellFactory(cellFactory);
        topicTable.getColumns().add(actionCol);
     }
     
     
     
    private void getAllTopic(){
        topicList =  TopicServiceImpl.getInstance().getAll();
       
        topicList.forEach(topic ->{
            int creatorId = topic.getCreatorId();
            Utilisateur creator = UtilisateurService.getInstance().getUserById(creatorId);
            topic.setCreator(creator);
            
        });
        
        data = FXCollections.observableArrayList(topicList);
       
        topicTable.setItems(data);
        
    }
    
    @FXML
    public void createTopic(ActionEvent e){
        
        showCreateEditDialog(null);
    }
    public void showCreateEditDialog(Topic topicToEdit){
        Stage createStage = new Stage();
        StackPane root = new StackPane();
        Scene scene = new Scene(root);
		
        TextField titleInput, tagInput;
        
        titleInput = new TextField();
        titleInput.setPromptText("Titre ");
        titleInput.setMaxWidth(300);

        TextArea descriptionInput;
        descriptionInput = new TextArea();
        descriptionInput.setTooltip(new Tooltip("Entrer la description"));
        descriptionInput.setPromptText("Description");
        descriptionInput.setMaxWidth(300);
        
        tagInput = new TextField();
        tagInput.setPromptText("Tag ");
        tagInput.setMaxWidth(300);

        MenuButton quizzMenu =  new MenuButton();
        quizzMenu.setMaxWidth(300);
        
        for(Quizz q : quizzList){
            MenuItem menuItem = new MenuItem(q.getName());
            
            menuItem.setUserData(q);
            menuItem.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    quizzMenu.setText(((Quizz)menuItem.getUserData()).getName());
                     quizzMenu.setUserData((Quizz)menuItem.getUserData());
                    selectQuizz((Quizz)menuItem.getUserData());
                }
            });
            if(topicToEdit.getQuizzId()!= q.getId() ){
                quizzMenu.setText(((Quizz)menuItem.getUserData()).getName());
                quizzMenu.setUserData((Quizz)menuItem.getUserData());
                selectQuizz((Quizz)menuItem.getUserData());
            }
           
            quizzMenu.getItems().add(menuItem);
            
        }
        
       
    if(topicToEdit != null){
        titleInput.setText(topicToEdit.getTitle());
        descriptionInput.setText(topicToEdit.getDescription());
        tagInput.setText(topicToEdit.getTag());
        
        
        
    }
        
        
        Button savebtn = new Button("Créer");
        savebtn.setTooltip(new Tooltip("Create a new topic"));

        if(topicToEdit == null){
            savebtn.setOnAction(event ->{
                 Topic newTopic = new Topic();
                newTopic.setTitle(titleInput.getText());
                newTopic.setDescription(descriptionInput.getText());
                newTopic.setTag(tagInput.getText());
                if(LoginnController.currentUser != null)
                    newTopic.setCreatorId(LoginnController.currentUser.getId());
                else
                    newTopic.setCreatorId(1);
                if(selectedQuizz!=null)
                    newTopic.setQuizzId(selectedQuizz.getId());
                TopicServiceImpl.getInstance().add(newTopic);
                getAllTopic();

                ((Node)(event.getSource())).getScene().getWindow().hide();
            });
        }else{
            savebtn.setOnAction(event ->{
                
                topicToEdit.setTitle(titleInput.getText());
                topicToEdit.setDescription(descriptionInput.getText());
                topicToEdit.setTag(tagInput.getText());
                
                if(selectedQuizz!=null)
                    topicToEdit.setQuizzId(selectedQuizz.getId());
                TopicServiceImpl.getInstance().edit(topicToEdit);
                getAllTopic();

                ((Node)(event.getSource())).getScene().getWindow().hide();
            });
        }
        
        VBox vbox = new VBox(10);
        vbox.getChildren().addAll(titleInput, descriptionInput,tagInput,quizzMenu, savebtn);
        vbox.setPadding(new Insets(10));		
        root.getChildren().add(vbox);

        createStage.setTitle("New Topic");
        createStage.setScene(scene);
        createStage.show();
    }
    
    
    private void refreshTableView(){
        topicList.addAll(TopicServiceImpl.getInstance().getAll());
         data = FXCollections.observableArrayList(topicList);
         topicTable.setItems(data);
    }
    
    private void selectQuizz(Quizz quizz){
        selectedQuizz = quizz;
    }
    private void redirectToTopicView(Topic currentTopic){
        
        try {
            URL url  = getClass().getClassLoader().getResource( "javafxapplication1/TopicView.fxml" );
            
            
            FXMLLoader loader = new FXMLLoader(url);

        // Create a controller instance
        TopicController controller = new TopicController();
        controller.currentTopic = currentTopic;
        controller.topicListTab = topicListTab;
        // Set it in the FXMLLoader
        loader.setController(controller);
            
            Parent root = loader.load();
            
            topicListTab.setContent(root);
                    
        } catch (IOException ex) {
            Logger.getLogger(TopicListController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void showConfirmationDialog(Topic selectedTopic){
        Topic currentTopic = selectedTopic;
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Deleting " + currentTopic.getTitle() + " " + currentTopic.getDescription());
        alert.setHeaderText("Are you Sure, You want to delete " + currentTopic.getTitle());
        alert.setContentText("This action can't be undone!");
        Optional<ButtonType> result = alert.showAndWait();

        if (result.get() == ButtonType.OK) {
            TopicServiceImpl.getInstance().delete(currentTopic.getId());
            getAllTopic();
        }
    }
    
    
    
}
