/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication1.esprit.controller;

import com.esprit.entity.Quizz;
import com.esprit.entity.Soutenance;
import com.esprit.entity.Topic;
import com.esprit.entity.Utilisateur;
import com.esprit.services.QuizzServiceImpl;
import com.esprit.services.SoutenanceService;
import com.esprit.services.TopicServiceImpl;
import com.esprit.services.UtilisateurService;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;

/**
 * FXML Controller class
 *
 * @author its48ha1
 */
public class ListeSoutenanceController implements Initializable {

    @FXML
    private TableView<Soutenance> soutenanceTable;
    @FXML
    private TableColumn<?, ?> id;
    @FXML
    private TableColumn<?, ?> topic;
    @FXML
    private TableColumn<?, ?> salle;
    @FXML
    private TableColumn<?, ?> result;
    @FXML
    private Button actualiser;
    @FXML
    private Button imprimer;
      public ObservableList<Soutenance> data = FXCollections.observableArrayList(
    
            
);

    /**
     * Initializes the controller class.
     */
    @Override
     public void initialize(URL url, ResourceBundle rb) {
        // TODO
        
        List<Soutenance> SoutenanceList = SoutenanceService.getInstance().listSoutenance();
       
       SoutenanceList.forEach(soutenance ->{
            
            
        });
        data = FXCollections.observableArrayList(SoutenanceList);
        
       id.setCellValueFactory(
            new PropertyValueFactory("id")
        );
        topic.setCellValueFactory(
            new PropertyValueFactory("topic")
        );
       salle.setCellValueFactory(
            new PropertyValueFactory("candidateId")
        );
       
        
        
        soutenanceTable.setItems(data);
        soutenanceTable.setRowFactory( tv -> {
            TableRow<Soutenance> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
            if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                Soutenance rowData = row.getItem();
                System.out.println(rowData);
               
                redirectToTopicView(rowData);
                

        
            }
            });
            return row ;
        });
    }    
    
     @FXML
    private void add(ActionEvent event) {
    }

    private void redirectToTopicView(Soutenance rowData) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
   
}
